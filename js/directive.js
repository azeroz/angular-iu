var app = angular.module('directive', []);

app.directive('iu', [function () {
	return {
		restrict: 'A',
		templateUrl: 'template.html'
	};
}])

app.directive('iuUpload', [function () {
	return {
		restrict: 'A',
		scope: true,
		controller: 'iuCtrl',
		link: function (scope, element, attrs) {

		}
	};
}])

app.directive('iuSingle', [function () {
	return {
		restrict: 'A',
		require: 'iuUpload',
		link: function (scope, element, attrs) {
			scope.iu.config.single = true;
		}
	};
}])

app.directive('iuBtn', [function () {
	return {
		require: '^iuUpload',
		restrict: 'A',
		link: function (scope, element, attrs) {
			var input;
			var target;
			if (typeof element.length === 'undefined'){
				element = [element];
			};
			angular.forEach(element, function(node){
				if (node.tagName === 'INPUT' && node.type==='file'){
					input = node;
					target = element;
				} else{
					input = document.createElement('input');
					input.setAttribute('type', 'file');
					input.style.visibility = 'hidden';
					input.style.position = 'absolute';
					node.appendChild(input);
					node.addEventListener('click', function(){
						input.click()
					}, false);
					target = $(input);
				}
			});
			target.bind('change',function(){
				scope.$apply(function(){
					scope.iu.uploadFiles(input);
				});
			});
			scope.$watch('iu.config',function(){
				if (scope.iu.config.single==false){
					input.setAttribute('multiple', 'multiple');
				};
			});
			if (attrs.iuDirectory){
				input.setAttribute('webkitdirectory', 'webkitdirectory');
			};
		}
	};
}])

app.directive('iuDrop', [function () {
	var preventEvent = function(event,callback){
		event.stopPropagation();
		event.preventDefault();
		if (callback) callback(event);
	};
	return {
		restrict: 'A',
		require: '^iuUpload',
		link: function (scope, element, attrs) {
			var dropEvent = function(e){
				return preventEvent(e,function(e){
					var data = e.dataTransfer;
					scope.iu.uploadFiles(data);
				});
			};
			if (typeof element.length === 'undefined'){
				element = [element];
			};
			angular.forEach(element, function(node){
				node.addEventListener('dragover', preventEvent, false);
		        node.addEventListener('dragenter', preventEvent, false);
		        node.addEventListener('drop', dropEvent, false);
			});
		}
	};
}])

app.directive('iuUrl', [function () {
	return {
		restrict: 'A',
		require: '^iuUpload',
		link: function (scope, element, attrs) {
			if (!scope.url) scope.url = [];
		}
	};
}])

app.directive('iuUrlBtn', [function () {
	return {
		restrict: 'A',
		require: '^iuUpload',
		link: function (scope, element, attrs) {
			var inputs = $('input[iu-url]');
			element.bind('click', function(){
				angular.forEach(inputs, function(input, index){
					if (input.value){
						scope.url[index] = input.value;
					} else{
						scope.url[index] = null;
					}
				});
				angular.forEach(scope.url,function(url, index){
					if (scope.url[index] == null){
						scope.url.splice(index,1);
					}
				});
				console.log(scope.url);
				// For later resolve
			});
		}
	};
}])

app.directive('iuCrop', [function () {
	return {
		restrict: 'EA',
		controller: 'iuCropCtrl',
		link: function (scope, element, attrs) {
			scope.crop.target = $(element).siblings('img');
		}
	};
}]);
