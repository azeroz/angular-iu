var app = angular.module('controller', []);

app.controller('iuCtrl', ['$scope', function ($scope) {
	$scope.iu = new IU();
	function IU(){
		this.config = {
			single: false
		};
		this.files = false;
		var iu = this;
		var toBase64 = function(files, callback){
			var done = 0;
			angular.forEach(files, function(file, index){
				var reader = new FileReader();
				reader.onloadend = function(){
					files[index].src = reader.result;
					done++;
					if (done==files.length){
						callback(files);	
					}
				};
				reader.readAsDataURL(files[index]);
			});
		};
		this.uploadFiles = function(input){
			var files = input.files;
			if (files.length == 0) return;
			if (!this.files){
				toBase64(files, function(b64Files){
					$scope.$apply(function(){
						iu.files = fileToArray(b64Files);
						inputReset(input);
					});
				});
			} else{
				var newFiles = fileDifference(files,this.files);
				if (newFiles){
					toBase64(newFiles, function(b64Files){
						$scope.$apply(function(){
							iu.files = iu.addFiles(iu.files,newFiles);
							inputReset(input);
						});
					});
				}
			};
		};
		this.addFiles = function(files,newFiles){
			if (this.config.single){
				return newFiles;
			} else{
				newFiles = fileToArray(newFiles);
				angular.forEach(newFiles, function(file,index){
					files.push(file);
				});
				return files;
			}
		};
		this.cancel = function(index){
			if (!isNaN(index)){
				if (this.files[index]){
					this.files.splice(index,1);
				};
				if (this.files.length == 0){
					this.files = false;
				}
			} else{
				this.files = false;
			}
		};
		var inputReset = function(input){
			input.value = '';
		}
		var fileToArray = function(files){
			var array = [];
			angular.forEach(files, function(file,index){
				array.push(file);
			});
			return array;
		};
		var fileDifference = function(newFiles,files){
			if (newFiles){
				var diff = [];
				angular.forEach(newFiles, function(file,index){
					if (!exist(file,files)){
						diff.push(file);
					}
				});
				return diff;
			} else{
				return false;
			}
		};
		var exist = function(aFile,files){
			var isExist = false;
			angular.forEach(files, function(file,index){
				if (fileEqual(aFile,file)){
					isExist = true;
					return true;
				}
			});
			if (isExist) return true;
			return false;
		}
		var fileEqual = function(a,b){
			if (a.name == b.name && a.size == b.size){
				return true;
			};
			return false;
		};
	};
}])

app.controller('iuCropCtrl', ['$scope', function ($scope) {
	$scope.crop = new Crop();
	function Crop(){
		this.status = false;
		this.fileIndex = NaN;
		this.target = false;
		this.jcrop = {};
		var coords = {};
		var getCoords = function(c){
			coords.x1 = c.x;
			coords.y1 = c.y;
			coords.x2 = c.x2;
			coords.y2 = c.y2;
			coords.w = c.w;
			coords.h = c.h;
		};
		var doCrop = function(image, coords){
			var c = document.createElement('canvas');
			var ctx = c.getContext("2d");
			var x1 = coords.x1;
			var y1 = coords.y1;
			var x2 = coords.x2;
			var y2 = coords.y2;
			var w = coords.w;
			var h = coords.h;
			c.width = w; c.height = h;
			ctx.clearRect(0,0,w,h);
			ctx.drawImage(image,x1,y1,w,h,0,0,w,h);
			return c.toDataURL();
		};
		this.enter = function(index){
			if (!this.target) return false;
			var jcrop = this.jcrop;
			this.status = true;
			this.fileIndex = index;
			console.log(this.target);
			this.target.Jcrop({
				onChange: getCoords,
				onSelect: getCoords,
				bgColor: 'black',
				bgOpacity: .4
			}, function(){
				jcrop = this;
			});
			this.jcrop = jcrop;
		};
		this.exit = function(){
			this.status = false;
			this.jcrop.release();
			this.jcrop.destroy();
		};
		this.start = function(){
			var target = this.target;
			var image = target[0];
			$scope.iu.files[this.fileIndex].src = doCrop(image, coords);
			this.exit();
			target.attr('style','');
		};
	};
}])